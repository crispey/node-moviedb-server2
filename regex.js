//
// ^ = start van string; $ = einde van de string
//

let validator = new RegExp('^1{2,4}$')
const demoString = '1111'
console.log(`strng = '${demoString}'`)

// RegExp functies
// console.log('exec  = ', validator.exec(demoString))
console.log('test  = voldoet', validator.test(demoString) ? 'WEL' : 'NIET')
// String functies
console.log('match = voldoet', demoString.match(validator) ? 'WEL' : 'NIET')

// Telefoonnummer:
// 06-12345678
// 0612345678
// 06 12345678
// - begint met 06
// - bestaat na 06 uit nog 8 cijfers
// - kan na 06 nog '-', ' ', '' bevatten

validator = new RegExp('^06(| |-)[0-9]{8}$')
let phone = '06-12345678'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
phone = '06 12345678'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
phone = '0612345678'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
phone = '06 1234567'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
phone = '06 123456789'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
phone = '06 abc456789'
console.log(phone, 'voldoet ', phone.match(validator) ? 'WEL' : 'NIET')
